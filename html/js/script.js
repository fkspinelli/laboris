$(document).ready(function(){
	$('#bannerHome .item').css({'height':$(window).height() - $('header').height() - 50 });

  $('#btn-search').click(function(){
    $('.form-search').slideToggle(function () {
      if ($(this).hasClass('show')) {
        $(this).removeClass('show');
        $('header .header-top .cancel').hide();
        $('header .header-top .search').show();
      } else {
        $(this).addClass('show');
        $('header .header-top .cancel').show();
        $('header .header-top .search').hide();
      }
    });
  });

  $('.row-equal').each(function(){
    var max = 0;
    $(this).find('.col-height').each(function(){
      var height = $(this).height();
      if (height > max) {
        max = height;
      }
    });
    $(this).find('.col-height').css({'height':max});
  });

	$('.slider-home .slider-home-slider').slick({
		prevArrow: $('.slider-home-left'),
      	nextArrow: $('.slider-home-right'),
      	dots: true,
      	appendDots: $('.slider-home-dots')
	});

	$('.slider-home-dots li button').each(function(){
		if (parseInt($(this).text()) < 10) {
			$(this).text( "0"+$(this).text() );
		}
	});

	$('.slider-customers').slick({
      	dots: true,
      	slidesToShow: 6,
      	slidesToScroll: 6,
      	arrows: false,
      	responsive: [
            {
              breakpoint: 1250,
              settings: {
                slidesToShow: 5
              }
            },
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 4
              }
            },
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 4
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 3
              }
            }
        ]
	});

	$('.scroll-page').click(function(event){
		event.preventDefault();
		var href = $(this).attr('href');
		$("html, body").animate({ scrollTop: $(href).offset().top }, 700);
	});

  $(".share-jssocial").jsSocials({
    showLabel: false,
    showCount: false,
    shares: ["facebook", "twitter", "linkedin", "whatsapp", "email"]
  });

  $('.scroll').jscroll({
      autoTrigger: false,
      nextSelector: '.next-page',
      contentSelector: '.scroll',
      loadingHtml: '<img src="/wp-content/themes/bgmrodotec/images/icon/reload.gif">',
      callback: function(){

      }
  });
  
});