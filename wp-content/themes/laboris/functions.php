<?php
// Adiciona o favicon no painel e personaliza a tela de login
require get_template_directory() . '/includes/customize-admin.php';

// Cria os campos no "Personalizar"
require get_template_directory() . '/includes/customizer.php';

// Remove acentuação dos arquivos no upload
add_filter('sanitize_file_name', 'remove_accents');

//after_setup_theme
function laboris_setup() {
	add_theme_support( 'title-tag' );

	add_theme_support( 'post-thumbnails' );
	add_image_size( 'slider', '9999', '9999', true );

	register_nav_menus( array(
		'menu-topo' => 'Menu Topo',
		'menu-rodape' => 'Menu Rodapé',
		'redes-sociais' => 'Redes Sociais',
	) );

	add_theme_support( 'custom-logo', array(
		'width' => 255,
		'height' => 73,
		'flex-width' => true,
		'header-text' => array( 'site-title' ),
	) );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
}
add_action( 'after_setup_theme', 'laboris_setup' );

//wp_enqueue_scripts
function laboris_init_scripts() {
	// Arquivos CSS
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . "/css/bootstrap.min.css" );
	wp_enqueue_style( 'slick', get_template_directory_uri() . "/css/slick.css" );
	wp_enqueue_style( 'bootstrap-select', get_template_directory_uri() . "/css/bootstrap-select.min.css" );
	wp_enqueue_style( 'jssocials', get_template_directory_uri() . "/css/jssocials.css" );
	wp_enqueue_style( 'jssocials-theme-flat', get_template_directory_uri() . "/css/jssocials-theme-flat.css" );
	wp_enqueue_style( 'style', get_template_directory_uri() . "/css/style.css" );
	wp_enqueue_style( 'responsive', get_template_directory_uri() . "/css/responsive.css" );

	// Javascripts
	wp_deregister_script( 'jquery' );
	wp_enqueue_script( 'jquery', get_template_directory_uri() . "/js/jquery.min.js", array(), false, true );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . "/js/bootstrap.min.js", array( 'jquery' ), false, true );
	wp_enqueue_script( 'bootstrap-select', get_template_directory_uri() . "/js/bootstrap-select.min.js", array( 'jquery' ), false, true );
	wp_enqueue_script( 'slick', get_template_directory_uri() . "/js/slick.min.js", array( 'jquery' ), false, true );
	wp_enqueue_script( 'jssocials', get_template_directory_uri() . "/js/jssocials.min.js", array( 'jquery' ), false, true );
	wp_enqueue_script( 'jscroll', get_template_directory_uri() . "/js/jscroll.js", array( 'jquery' ), false, true );
	wp_enqueue_script( 'script', get_template_directory_uri() . "/js/script.js", array( 'jquery' ), false, true );
}
add_action( 'wp_enqueue_scripts', 'laboris_init_scripts', 11 );

//document_title_separator
function laboris_document_title_separator( $separator ) {
	return '|';
}
add_filter( 'document_title_separator', 'laboris_document_title_separator' );


add_action( 'init', 'create_post_type' );

function create_post_type() {
  	register_post_type( 'noticias',
     	array(
			'labels' => array(
			'name' => __( 'Notícias' ),
			'singular_name' => __( 'Notícia' ),
			'add_new_item'  => __( 'Adicionar notícia'),
       	),
			'public' => true,
			'has_archive' => true,
			'menu_position' => 15,
			'supports' => array('title','excerpt','editor','thumbnail')
     	)
   	);
}

//Quando der problema no post type acionar esta função
flush_rewrite_rules();

add_action( 'init', 'custom_taxonomy', 0 );

function custom_taxonomy() {

	// categoria noticias
 	$labels_noticia = array(
	    'name'                       => _x( 'Categorias da notícia', 'Taxonomy General Name', 'text_domain' ),
	    'singular_name'              => _x( 'Categorias da notícia', 'Taxonomy Singular Name', 'text_domain' ),
	    'menu_name'                  => __( 'Categorias da notícia', 'text_domain' )
	    );
 	$args_noticia = array(
	    'hierarchical' => true,
	    'labels'                     => $labels_noticia,
	    'show_ui' => true,
	    'public'                     => true,
	    'show_admin_column'          => true,
	    'show_in_nav_menus'          => true,
	    'rewrite' => array( 'slug' => 'categoria-noticias' ),
  	);
  	register_taxonomy('noticiacat', array('noticias') , $args_noticia);
}


function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');