<!DOCTYPE html>
<html lang="pt-br">
 	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Laboris</title>
		<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/img/favicon.png" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<?php wp_head(); ?>
  	</head>
	<body <?php body_class(); ?>>
	    <header>
	      <div class="container-fluid">
	        <input type="checkbox" id="menu" style="position: absolute; visibility: hidden; top: -9999; left: -9999;">
	        <div class="header-top">
	          <div class="row">
	            <div class="col-xs-1">
	              <label for="menu">
	                <svg class="bars" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;">
	                  <g><g><path d="M492,236H20c-11.046,0-20,8.954-20,20c0,11.046,8.954,20,20,20h472c11.046,0,20-8.954,20-20S503.046,236,492,236z" fill="#D80027"/></g></g>
	                  <g><g><path d="M492,76H20C8.954,76,0,84.954,0,96s8.954,20,20,20h472c11.046,0,20-8.954,20-20S503.046,76,492,76z" fill="#D80027"/></g></g>
	                  <g><g><path d="M492,396H20c-11.046,0-20,8.954-20,20c0,11.046,8.954,20,20,20h472c11.046,0,20-8.954,20-20    C512,404.954,503.046,396,492,396z" fill="#D80027"/></g></g>
	                </svg>
	              </label>
	            </div>
	            <div class="col-xs-10">
	              <a href="<?php echo get_theme_mod( 'topo_logo_link' ); ?>">
		              <img src="<?php echo get_theme_mod( 'topo_logo' ); ?>" class="logo">
		              <img src="<?php echo get_theme_mod( 'topo_logo_white' ); ?>" class="logo open">
	              </a>
	            </div>
	            <div class="col-xs-1">
	              <a href="#" id="btn-search">
	                <img src="<?php echo bloginfo('template_url'); ?>/img/search.svg" class="search">
	                <img src="<?php echo bloginfo('template_url'); ?>/img/cancel-red.svg" class="cancel">
	              </a>
	            </div>
	          </div>
	        </div>
	        <div class="header-bottom">
	          <div class="row">
	            <div class="col-xs-10 col-xs-push-1">
	              <nav>
	                <ul>
						<?php
				 			wp_nav_menu( array(
				 				'theme_location' => 'menu-topo',
				 				'container' => false,
				 				'items_wrap' => '%3$s',
			 					'fallback_cb' => false,
				 			) );
				 		?>
	                </ul>
	              </nav>
	            </div>
	          </div>
	        </div>
	        <div class="form-search">
          <div class="row">
            <div class="col-sm-6 col-sm-push-3">
              <form method="get" action="<?php bloginfo('url'); ?>">
                <span class="line"></span>
                <input name="s" value="<?php the_search_query(); ?>" type="text" class="form-control" placeholder="Buscar na Laboris">
              </form>
            </div>
          </div>
        </div>
	      </div>
	    </header>