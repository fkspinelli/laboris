
    <footer>
      <div class="container-fluid">
        <div class="footer-top">
          <div class="social-network">
            <ul>
              <?php
                wp_nav_menu( array(
                  'theme_location' => 'redes-sociais',
                  'container' => false,
                  'items_wrap' => '%3$s',
                  'fallback_cb' => false,
                ) );
              ?>
            </ul>
          </div>
          <div class="menu">
            <nav>
              <ul>
                <?php
                  wp_nav_menu( array(
                    'theme_location' => 'menu-rodape',
                    'container' => false,
                    'items_wrap' => '%3$s',
                    'fallback_cb' => false,
                  ) );
                ?>
              </ul>
            </nav>
          </div>
        </div>
        <div class="footer-bottom">
          <?php echo get_theme_mod( 'rodape_copyright' ); ?> - Desenvolvido por <a href="http://dizain.com.br/" target="_blank">Diz’ain</a>
        </div>
      </div>
    </footer>

    <?php wp_footer(); ?>
  </body>
</html>