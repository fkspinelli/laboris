<?php  
/*
* Template Name: Competências
*/
get_header();
?>

    <div class="top-title-page">
      <div class="top-title text-center">
        <h1><?php the_title(); ?></h1>
      </div>
    </div>

    <div class="banner banner-filter parceiro" style="background-image: url(<?php echo CFS()->get('imagem'); ?>);">
      <div class="banner-caption">
        <?php if (CFS()->get('titulo')): ?>
        <div class="banner-text">
          <span class="line"></span>
          <h1><?php echo CFS()->get('titulo'); ?></h1>
        </div>
        <?php endif; ?>
      </div>
      <img src="<?php bloginfo('template_url'); ?>/img/b-i-h.jpg">
    </div>


    <section>
      <div class="container-fluid quem-somos competencias">

        <div class="box-form-contato parceiro">
          <div class="row">
            <div class="col-sm-10 col-sm-push-1">
              <div class="box-quem-somos">
                <ul class="nav nav-pills">
                  <li class="active"><a data-toggle="pill" href="#consultoria"><span>Consultoria</span></a></li>
                  <li><a data-toggle="pill" href="#tecnologia"><span>Tecnologia</span></a></li>
                  <li><a data-toggle="pill" href="#outsourcing"><span>Outsourcing</span></a></li>
                </ul>

                <div class="panel-group" id="accordion">
                  <div class="tab-content">
                    <div id="consultoria" class="tab-pane fade in active">


                        <?php 
                        $itens = CFS()->get('itens_consultoria');
                        if ($itens):
                          foreach ( $itens as $item ):
                        ?>
                        <div class="item">

                          <?php if ($item['titulo_item_consultoria']): ?>
                          <h2><?php echo $item['titulo_item_consultoria']; ?></h2>
                          <?php endif; ?>

                          <?php if ($item['texto_item_consultoria']): ?>
                          <?php echo $item['texto_item_consultoria']; ?>
                          <?php endif; ?>

                          <?php 
                          $subitens = $item['subitens_consultoria'];
                          if ($subitens):
                            $i = 0;
                            foreach ( $subitens as $subitem ):
                          ?>
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <h3 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse-consultoria-<?php echo $i; ?>"><?php echo $subitem['titulo_subitem_consultoria']; ?></a>
                              </h3>
                            </div>
                            <div id="collapse-consultoria-<?php echo $i; ?>" class="panel-collapse collapse">
                              <div class="panel-body">
                                <?php echo $subitem['texto_subitem_consultoria']; ?>
                              </div>
                            </div>
                          </div>
                          <?php
                            $i++;
                            endforeach;
                          endif;
                          ?>


                        </div>
                        <?php
                          endforeach;
                        endif;
                        ?>

                    </div>
                    <div id="tecnologia" class="tab-pane fade">
                      
                      <?php 
                        $itens = CFS()->get('itens_tecnologia');
                        if ($itens):
                          foreach ( $itens as $item ):
                        ?>
                        <div class="item">

                          <?php if ($item['titulo_item_tecnologia']): ?>
                          <h2><?php echo $item['titulo_item_tecnologia']; ?></h2>
                          <?php endif; ?>

                          <?php if ($item['texto_item_tecnologia']): ?>
                          <?php echo $item['texto_item_tecnologia']; ?>
                          <?php endif; ?>

                          <?php 
                          $subitens = $item['subitens_tecnologia'];
                          if ($subitens):
                            $i = 0;
                            foreach ( $subitens as $subitem ):
                          ?>
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <h3 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse-tecnologia-<?php echo $i; ?>"><?php echo $subitem['titulo_subitem_tecnologia']; ?></a>
                              </h3>
                            </div>
                            <div id="collapse-tecnologia-<?php echo $i; ?>" class="panel-collapse collapse">
                              <div class="panel-body">
                                <?php echo $subitem['texto_subitem_tecnologia']; ?>
                              </div>
                            </div>
                          </div>
                          <?php
                            $i++;
                            endforeach;
                          endif;
                          ?>


                        </div>
                        <?php
                          endforeach;
                        endif;
                        ?>

                    </div>
                    <div id="outsourcing" class="tab-pane fade">
                      
                      <?php 
                        $itens = CFS()->get('itens_outsourcing');
                        if ($itens):
                          foreach ( $itens as $item ):
                        ?>
                        <div class="item">

                          <?php if ($item['titulo_item_outsourcing']): ?>
                          <h2><?php echo $item['titulo_item_outsourcing']; ?></h2>
                          <?php endif; ?>

                          <?php if ($item['texto_item_outsourcing']): ?>
                          <?php echo $item['texto_item_outsourcing']; ?>
                          <?php endif; ?>

                          <?php 
                          $subitens = $item['subitens_outsourcing'];
                          if ($subitens):
                            $i = 0;
                            foreach ( $subitens as $subitem ):
                          ?>
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <h3 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse-outsourcing-<?php echo $i; ?>"><?php echo $subitem['titulo_subitem_outsourcing']; ?></a>
                              </h3>
                            </div>
                            <div id="collapse-outsourcing-<?php echo $i; ?>" class="panel-collapse collapse">
                              <div class="panel-body">
                                <?php echo $subitem['texto_subitem_outsourcing']; ?>
                              </div>
                            </div>
                          </div>
                          <?php
                            $i++;
                            endforeach;
                          endif;
                          ?>


                        </div>
                        <?php
                          endforeach;
                        endif;
                        ?>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <?php get_template_part('includes/content','newsletter'); ?>

<?php get_footer(); ?>