<?php  
/*
* Template Name: Quem Somos
*
*/
get_header();
the_post();
?>

    <div class="top-title-page">
      <div class="top-title text-center">
        <h1><?php the_title(); ?></h1>
      </div>
    </div>

    <div class="banner banner-filter parceiro" style="background-image: url(<?php echo CFS()->get('imagem_banner'); ?>);">
      <div class="banner-caption">

        <?php if (CFS()->get('texto_banner')): ?>
        <div class="banner-text">
          <span class="line"></span>
          <h1><?php echo CFS()->get('texto_banner'); ?></h1>
        </div>
      <?php endif; ?>

      </div>
      <img src="<?php echo bloginfo('template_url'); ?>/img/b-i-h.jpg">
    </div>


    <section>
      <div class="container-fluid quem-somos">

        <?php if(CFS()->get('texto_geral')): ?>
        <div class="box-form-contato parceiro">
          <div class="row">
            <div class="col-sm-10 col-sm-push-1">
              <div class="box-quem-somos">
                <?php echo CFS()->get('texto_geral'); ?>
              </div>
            </div>
          </div>
        </div>
        <?php endif; ?>


        <div class="valores-missao">
          <div class="row row-equal">

            <?php if (CFS()->get('valores')): ?>
            <div class="col-md-5 col-md-push-1 col-height">
              <div class="box">

                <?php if (CFS()->get('titulo_valores')): ?>
                <div class="top-title text-center">
                  <h2><?php echo CFS()->get('titulo_valores'); ?></h2>
                </div>
                <?php endif; ?>

                <?php echo CFS()->get('valores'); ?>
              </div>
            </div>
            <?php endif; ?>

            <?php if (CFS()->get('missao')): ?>
            <div class="col-md-5 col-md-push-1 col-height">
              <div class="box">

                <?php if (CFS()->get('titulo_missao')): ?>
                <div class="top-title text-center">
                  <h2><?php echo CFS()->get('titulo_missao'); ?></h2>
                </div>
                <?php endif; ?>

                <?php echo CFS()->get('missao'); ?>
              </div>
            </div>
            <?php endif; ?>

          </div>
        </div>


        <div class="perfil">
          <div class="row row-equal">

            <?php if (CFS()->get('texto_1')): ?>
            <div class="col-md-5 col-md-push-1 col-height item">

              <?php if (CFS()->get('nome_1')): ?>
              <div class="top-title text-center">
                <h2><?php echo CFS()->get('nome_1'); ?></h2>
              </div>
              <?php endif; ?>

              <div class="box">
                <img src="<?php echo CFS()->get('foto_1'); ?>">
                <?php echo CFS()->get('texto_1'); ?>
              </div>
            </div>
            <?php endif; ?>

            <?php if (CFS()->get('texto_2')): ?>
            <div class="col-md-5 col-md-push-1 col-height item">

              <?php if (CFS()->get('nome_2')): ?>
              <div class="top-title text-center">
                <h2><?php echo CFS()->get('nome_2'); ?></h2>
              </div>
              <?php endif; ?>

              <div class="box">
                <img src="<?php echo CFS()->get('foto_2'); ?>">
                <?php echo CFS()->get('texto_2'); ?>
              </div>
            </div>
            <?php endif; ?>

          </div>
        </div>

      </div>
    </section>

    <div style="margin-bottom: 150px; "></div>

    <?php get_template_part('includes/content','newsletter'); ?>

<?php get_footer(); ?>