<?php  
/*
* Template Name: Como Fazemos
*
*/
get_header();
?>

    <div class="top-title-page">
      <div class="top-title text-center">
        <h1><?php the_title(); ?></h1>
      </div>
    </div>

    <div class="banner banner-filter parceiro" style="background-image: url(<?php echo CFS()->get('banner'); ?>);">
      <div class="banner-caption">
        <div class="banner-text">
          <span class="line"></span>

          <?php if (CFS()->get('titulo')): ?>
          <h1><?php echo CFS()->get('titulo'); ?></h1>
          <?php endif; ?>

          <?php if (CFS()->get('subtitulo')): ?>
          <h2><?php echo CFS()->get('subtitulo'); ?></h2>
          <?php endif; ?>

        </div>
      </div>
      <img src="<?php bloginfo('template_url'); ?>/img/b-i-h.jpg">
    </div>


    <section>
      <div class="container-fluid quem-somos como-fazemos">

        <div class="box-form-contato parceiro">
          <div class="row">
            <div class="col-sm-10 col-sm-push-1">
              <div class="box-quem-somos">
                <div class="grafico">
                  <?php if (CFS()->get('titulo_grafico')): ?>
                  <h3><?php echo CFS()->get('titulo_grafico'); ?></h3>
                  <?php endif; ?>

                  <img src="<?php echo CFS()->get('imagem'); ?>">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="box-circles">
          <div class="top-title text-center">
            <?php if (CFS()->get('titulo_consultoria')): ?>
            <h3><?php echo CFS()->get('titulo_consultoria'); ?></h3>
            <?php endif; ?>
            <div class="box">
              <div class="row">
                <div class="col-sm-10 col-sm-push-1">
                  <?php 
                  $boxes = CFS()->get('box');
                  if ($boxes):
                    foreach ( $boxes as $box ):
                  ?>
                  <div class="box-col">
                    <div class="circle">
                      <img src="<?php echo $box['imagem_de_fundo']; ?>">
                      <div class="content">
                        <img src="<?php echo $box['icone']; ?>">
                        <p><?php echo $box['texto']; ?></p>
                      </div>
                    </div>
                  </div>
                  <?php
                    endforeach;
                  endif;
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="box-text">
          <div class="row">
            <div class="col-sm-10 col-sm-push-1">
              <?php echo CFS()->get('texto_1'); ?>
            </div>
          </div>
        </div>

        <div class="box-text" style="padding: 0;">
          <div class="row">
            <div class="col-sm-10 col-sm-push-1">

            <?php if (CFS()->get('titulo_texto_2')): ?>
            <div class="top-title text-center">
              <h3><?php echo CFS()->get('titulo_texto_2'); ?></h3>
            </div>
            <?php endif; ?>

            </div>
          </div>
          <div class="box">
            <div class="row">
              <div class="col-sm-10 col-sm-push-1">
                <?php echo CFS()->get('texto_2'); ?>
              </div>
            </div>
            <div class="row" style="padding-top: 60px;">
              <div class="col-sm-6 col-sm-push-3">
                <a href="<?php echo CFS()->get('link_do_botao'); ?>" class="btn btn-danger btn-outline btn-block text-uppercase"><span class="hidden-xs">CONHEÇA</span> NOSSAS COMPETÊNCIAS</a>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section>

    <?php get_template_part('includes/content','newsletter'); ?>

<?php get_footer(); ?>