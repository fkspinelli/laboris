<?php  
/*
* Template Name: Clientes
*
*/
get_header();
the_post();
?>

    <div class="top-title-page">
      <div class="top-title text-center">
        <h1><?php the_title(); ?></h1>
      </div>
    </div>

    <section>
      <div class="container-fluid">

        <?php if (CFS()->get('subtitulo')): ?>
        <div class="top-title no-line text-center">
          <h2><?php echo CFS()->get('subtitulo'); ?></h2>
        </div>
        <?php endif; ?>

        <!-- grid desktop -->
        <div class="clientes hidden-xs">

          <?php
          $clientes = CFS()->get('clientes');
          if ($clientes):
            $i = 0;
            foreach ( $clientes as $cliente ):
          ?>

            <?php if( ($i % 30 == 0) ): ?>
            <div class="clientes-content">
              <div class="row">
            <?php endif; ?>

              <?php if( ($i % 30 >= 0) && ($i % 30 <= 3)): ?>
                <?php if( ($i % 30 == 0) ): ?>
                <div class="col-xs-2">
                  <div class="v-align">
                <?php endif; ?>
                  <div class="cliente" style="background-image: url(<?php echo $cliente['logo']; ?>);">
                    <img src="<?php bloginfo('template_url'); ?>/img/quadrado.jpg">
                  </div>
                <?php if( ($i % 30 == 3) ): ?>
                  </div>
                </div>
                <?php endif; ?>
              <?php endif; ?>


              <?php if( ($i % 30 >= 4) && ($i % 30 <= 8)): ?>
                <?php if( ($i % 30 == 4) ): ?>
                <div class="col-xs-2">
                  <div class="v-align">
                <?php endif; ?>
                  <div class="cliente" style="background-image: url(<?php echo $cliente['logo']; ?>);">
                    <img src="<?php bloginfo('template_url'); ?>/img/quadrado.jpg">
                  </div>
                <?php if( ($i % 30 == 8) ): ?>
                  </div>
                </div>
                <?php endif; ?>
              <?php endif; ?>


              <?php if( ($i % 30 >= 9) && ($i % 30 <= 14)): ?>
                <?php if( ($i % 30 == 9) ): ?>
                <div class="col-xs-2">
                  <div class="v-align">
                <?php endif; ?>
                  <div class="cliente" style="background-image: url(<?php echo $cliente['logo']; ?>);">
                    <img src="<?php bloginfo('template_url'); ?>/img/quadrado.jpg">
                  </div>
                <?php if( ($i % 30 == 14) ): ?>
                  </div>
                </div>
                <?php endif; ?>
              <?php endif; ?>


              <?php if( ($i % 30 >= 15) && ($i % 30 <= 20)): ?>
                <?php if( ($i % 30 == 15) ): ?>
                <div class="col-xs-2">
                  <div class="v-align">
                <?php endif; ?>
                  <div class="cliente" style="background-image: url(<?php echo $cliente['logo']; ?>);">
                    <img src="<?php bloginfo('template_url'); ?>/img/quadrado.jpg">
                  </div>
                <?php if( ($i % 30 == 20) ): ?>
                  </div>
                </div>
                <?php endif; ?>
              <?php endif; ?>


              <?php if( ($i % 30 >= 21) && ($i % 30 <= 25)): ?>
                <?php if( ($i % 30 == 21) ): ?>
                <div class="col-xs-2">
                  <div class="v-align">
                <?php endif; ?>
                  <div class="cliente" style="background-image: url(<?php echo $cliente['logo']; ?>);">
                    <img src="<?php bloginfo('template_url'); ?>/img/quadrado.jpg">
                  </div>
                <?php if( ($i % 30 == 25) ): ?>
                  </div>
                </div>
                <?php endif; ?>
              <?php endif; ?>


              <?php if( ($i % 30 >= 26) && ($i % 30 <= 29)): ?>
                <?php if( ($i % 30 == 26) ): ?>
                <div class="col-xs-2">
                  <div class="v-align">
                <?php endif; ?>
                  <div class="cliente" style="background-image: url(<?php echo $cliente['logo']; ?>);">
                    <img src="<?php bloginfo('template_url'); ?>/img/quadrado.jpg">
                  </div>
                <?php if( ($i % 30 == 29) ): ?>
                  </div>
                </div>
                <?php endif; ?>
              <?php endif; ?>


            <?php if( ($i % 30 == 29) ): ?>
              </div>
            </div>
            <?php endif; ?>

          <?php
            $i++;
            endforeach;
          endif;
          ?>

        </div></div></div></div></div>
        <!-- /grid desktop -->






        <!-- grid mobile -->
        <div class="clientes visible-xs">

          <?php
          $clientes = CFS()->get('clientes');
          if ($clientes):
            $i = 0;
            foreach ( $clientes as $cliente ):
          ?>

            <?php if( ($i % 14 == 0) ): ?>
            <div class="clientes-content">
              <div class="row">
            <?php endif; ?>

              <?php if( ($i % 14 >= 0) && ($i % 14 <= 2)): ?>
                <?php if( ($i % 14 == 0) ): ?>
                <div class="col-xs-3">
                  <div class="v-align">
                <?php endif; ?>
                  <div class="cliente" style="background-image: url(<?php echo $cliente['logo']; ?>);">
                    <img src="<?php bloginfo('template_url'); ?>/img/quadrado.jpg">
                  </div>
                <?php if( ($i % 14 == 2) ): ?>
                  </div>
                </div>
                <?php endif; ?>
              <?php endif; ?>


              <?php if( ($i % 14 >= 3) && ($i % 14 <= 6)): ?>
                <?php if( ($i % 14 == 3) ): ?>
                <div class="col-xs-3">
                  <div class="v-align">
                <?php endif; ?>
                  <div class="cliente" style="background-image: url(<?php echo $cliente['logo']; ?>);">
                    <img src="<?php bloginfo('template_url'); ?>/img/quadrado.jpg">
                  </div>
                <?php if( ($i % 14 == 6) ): ?>
                  </div>
                </div>
                <?php endif; ?>
              <?php endif; ?>


              <?php if( ($i % 14 >= 7) && ($i % 14 <= 10)): ?>
                <?php if( ($i % 14 == 7) ): ?>
                <div class="col-xs-3">
                  <div class="v-align">
                <?php endif; ?>
                  <div class="cliente" style="background-image: url(<?php echo $cliente['logo']; ?>);">
                    <img src="<?php bloginfo('template_url'); ?>/img/quadrado.jpg">
                  </div>
                <?php if( ($i % 14 == 10) ): ?>
                  </div>
                </div>
                <?php endif; ?>
              <?php endif; ?>


              <?php if( ($i % 14 >= 11) && ($i % 14 <= 13)): ?>
                <?php if( ($i % 14 == 11) ): ?>
                <div class="col-xs-3">
                  <div class="v-align">
                <?php endif; ?>
                  <div class="cliente" style="background-image: url(<?php echo $cliente['logo']; ?>);">
                    <img src="<?php bloginfo('template_url'); ?>/img/quadrado.jpg">
                  </div>
                <?php if( ($i % 14 == 13) ): ?>
                  </div>
                </div>
                <?php endif; ?>
              <?php endif; ?>


            <?php if( ($i % 14 == 13) ): ?>
              </div>
            </div>
            <?php endif; ?>

          <?php
            $i++;
            endforeach;
          endif;
          ?>

        </div>


      </div>
    </section>



    <?php get_template_part('includes/content','newsletter'); ?>

<?php get_footer(); ?>