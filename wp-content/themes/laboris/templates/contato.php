<?php  
/*
* Template Name: Contato
*
*/
get_header();
the_post();
?>

    <div class="top-title-page">
      <div class="top-title text-center">
        <h1><?php the_title(); ?></h1>
      </div>
    </div>

    <section>
      <div class="container-fluid">
        <div class="top-title no-line text-center">
          <h2><?php echo CFS()->get('subtitulo'); ?></h2>
        </div>

        <div class="box-form-contato" style="background-image: url(<?php echo CFS()->get('banner'); ?>);"">
          <div class="row">
            <div class="col-sm-6 col-sm-push-3">
              <?php the_content(); ?>
            </div>
          </div>
          <div class="local">
          	<?php if (CFS()->get('titulo_do_banner')):  ?>
            <b><?php echo CFS()->get('titulo_do_banner'); ?></b> <br>
        	<?php endif;  ?>
            
            <?php if (CFS()->get('subtitulo_do_banner')):  ?>
            	<?php echo CFS()->get('subtitulo_do_banner'); ?>
        	<?php endif;  ?>
          </div>
        </div>

        <div class="maps">
        	<ul class="nav nav-pills">
        	<?php 
        	$locais = CFS()->get('local');
        	if ($locais):
        		$i = 0;
        		foreach ( $locais as $local ):
			?>
				<li class="<?php if($i == 0){echo "active";} ?>"><a data-toggle="pill" href="#<?php echo $local['uf']; ?>"><?php echo $local['uf']; ?></a></li>
			<?php
				$i++;
            	endforeach;
        	endif;  
        	?>
        	</ul>

			<div class="tab-content">

			<?php 
			$locais = CFS()->get('local');
			if ($locais):
				$i = 0;
				foreach ( $locais as $local ):
			?>
				<div id="<?php echo $local['uf']; ?>" class="tab-pane fade <?php if($i == 0){echo "in active";} ?> ">
					<?php echo $local['mapa']; ?>
				</div>

			<?php
					$i++;
				endforeach;
			endif;  
			?>
			</div>
        </div>

      </div>
    </section>

    <?php get_template_part('includes/content','newsletter'); ?>

<?php get_footer(); ?>