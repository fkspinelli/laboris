<?php get_header(); ?>

    <div id="bannerHome" class="carousel slide carousel-fade" data-ride="carousel">
      <div class="banner-botton">
        <ol class="carousel-indicators">
          <?php 
          $banners = CFS()->get('banners');
          if ($banners):
            $i = 0;
            foreach ( $banners as $banner ):
          ?>
          <li data-target="#bannerHome" data-slide-to="<?php echo $i; ?>" class="<?php echo $i==0 ? 'active' : null; ?>"></li>
          <?php
            $i++;
            endforeach;
          endif;
          ?>
        </ol>
        <div class="scroll-down">
          Descubra
          <a href="#title-articles" class="scroll-page">
            <img src="<?php echo bloginfo('template_url'); ?>/img/down-chevron.svg">
          </a>
        </div>
      </div>
      <div class="carousel-inner">
        <?php 
        $banners = CFS()->get('banners');
        if ($banners):
          $i = 0;
          foreach ( $banners as $banner ):
        ?>
        <div class="item <?php echo $i==0 ? 'active' : null; ?>" style="background-image: url('<?php echo $banner['imagem_banner']; ?>');">
          <div class="carousel-caption">
            <div class="circle"></div>
            <div class="carousel-text">
              <span class="line"></span>

              <?php if(!empty($banner['titulo_banner'])): ?>
              <h1><?php echo $banner['titulo_banner']; ?></h1>
              <?php endif; ?>

              <?php if(!empty($banner['subtitulo_banner'])): ?>
              <h2><?php echo $banner['subtitulo_banner']; ?></h2>
              <?php endif; ?>

            </div>
          </div>
        </div>
        <?php
          $i++;
          endforeach;
        endif;
        ?>
      </div>
      <a class="left carousel-control" href="#bannerHome" data-slide="prev">
        <img src="<?php echo bloginfo('template_url'); ?>/img/down-arrow.svg" class="rotate90">
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#bannerHome" data-slide="next">
        <img src="<?php echo bloginfo('template_url'); ?>/img/down-arrow.svg" class="rotate-90">
        <span class="sr-only">Next</span>
      </a>
    </div>

    <?php if(!empty(CFS()->get('titulo_noticias'))): ?>
    <div id="title-articles" class="top-title text-center">
      <h2><?php echo CFS()->get('titulo_noticias'); ?></h2>
    </div>
    <?php endif; ?>

    <?php get_template_part('includes/content','noticias'); ?>

    <section>
      <div class="container-fluid">
        <!-- <div class="slider-home">
          <div></div>
        </div> -->
        <div class="slider-home">
          <div class="slider-home-top">
            <div class="row">
              <div class="col-sm-5 col-sm-push-6">
                <div class="slider-home-content hidden-sm hidden-xs ">
                  <div class="top-title text-left">
                    <h2>O que o novo consumidor espera da sua empresa:</h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="slider-home-bottom">
            <div class="row">
              <div class="col-md-6">
                <div class="box-circle">
                  <div class="circle">
                    <div class="slider-home-dots"></div>
                  </div>
                  <img src="<?php echo bloginfo('template_url'); ?>/img/foto5.jpg">
                </div>
              </div>
              <div class="col-md-5">


                <div class="slider-home-content visible-sm visible-xs ">
                  <div class="top-title text-left">
                    <h2>O que o novo consumidor espera da sua empresa:</h2>
                  </div>
                </div>

                <div class="slider-home-content">
                  <div class="slider-home-slider">
                    <div>
                      <h3>Colaboração</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris viverra luctus mi. Vivamus vitae leo vel est ullamcorper tincidunt. Curabitur sed feugiat lectus, in placerat quam. Praesent rhoncus lectus eget vestibulum fermentum. Sed scelerisque ex sit amet tincidunt euismod, dui velit egestas elit, ultricies interdum orci lorem ac enim. Nulla facilisi. Cras pulvinar egestas tellus. Proin eleifend, purus eget varius tincidunt, ante ligula imperdiet risus, at pulvinar nibh justo non diam.</p>
                    </div>
                    <div>
                      <h3>Colaboração</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris viverra luctus mi. Vivamus vitae leo vel est ullamcorper tincidunt. Curabitur sed feugiat lectus, in placerat quam. Praesent rhoncus lectus eget vestibulum fermentum. Sed scelerisque ex sit amet tincidunt euismod, dui velit egestas elit, ultricies interdum orci lorem ac enim. Nulla facilisi. Cras pulvinar egestas tellus. Proin eleifend, purus eget varius tincidunt, ante ligula imperdiet risus, at pulvinar nibh justo non diam.</p>
                    </div>
                    <div>
                      <h3>Colaboração</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris viverra luctus mi. Vivamus vitae leo vel est ullamcorper tincidunt. Curabitur sed feugiat lectus, in placerat quam. Praesent rhoncus lectus eget vestibulum fermentum. Sed scelerisque ex sit amet tincidunt euismod, dui velit egestas elit, ultricies interdum orci lorem ac enim. Nulla facilisi. Cras pulvinar egestas tellus. Proin eleifend, purus eget varius tincidunt, ante ligula imperdiet risus, at pulvinar nibh justo non diam.</p>
                    </div>
                    <div>
                      <h3>Colaboração</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris viverra luctus mi. Vivamus vitae leo vel est ullamcorper tincidunt. Curabitur sed feugiat lectus, in placerat quam. Praesent rhoncus lectus eget vestibulum fermentum. Sed scelerisque ex sit amet tincidunt euismod, dui velit egestas elit, ultricies interdum orci lorem ac enim. Nulla facilisi. Cras pulvinar egestas tellus. Proin eleifend, purus eget varius tincidunt, ante ligula imperdiet risus, at pulvinar nibh justo non diam.</p>
                    </div>
                    <div>
                      <h3>Colaboração</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris viverra luctus mi. Vivamus vitae leo vel est ullamcorper tincidunt. Curabitur sed feugiat lectus, in placerat quam. Praesent rhoncus lectus eget vestibulum fermentum. Sed scelerisque ex sit amet tincidunt euismod, dui velit egestas elit, ultricies interdum orci lorem ac enim. Nulla facilisi. Cras pulvinar egestas tellus. Proin eleifend, purus eget varius tincidunt, ante ligula imperdiet risus, at pulvinar nibh justo non diam.</p>
                    </div>
                    <div>
                      <h3>Colaboração</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris viverra luctus mi. Vivamus vitae leo vel est ullamcorper tincidunt. Curabitur sed feugiat lectus, in placerat quam. Praesent rhoncus lectus eget vestibulum fermentum. Sed scelerisque ex sit amet tincidunt euismod, dui velit egestas elit, ultricies interdum orci lorem ac enim. Nulla facilisi. Cras pulvinar egestas tellus. Proin eleifend, purus eget varius tincidunt, ante ligula imperdiet risus, at pulvinar nibh justo non diam.</p>
                    </div>
                  </div>
                </div>
                <div class="slider-home-nav">
                  <div class="slider-home-left">
                    <img src="<?php echo bloginfo('template_url'); ?>/img/down-arrow-red.svg" class="rotate90">
                  </div>
                  <div class="slider-home-right">
                    <img src="<?php echo bloginfo('template_url'); ?>/img/down-arrow-red.svg" class="rotate-90">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="customers-home">
          <div class="top-title text-center">
            <h2>Alguns de nossos clientes</h2>
          </div>
          <div class="slider-customers">
            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/globo.png" class="img-responsive">
            </div>
            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/americanas.png" class="img-responsive">
            </div>
            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/esso.png" class="img-responsive">
            </div>
            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/band.png" class="img-responsive">
            </div>
            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/hsbc.png" class="img-responsive">
            </div>
            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/losango.png" class="img-responsive">
            </div>

            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/globo.png" class="img-responsive">
            </div>
            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/americanas.png" class="img-responsive">
            </div>
            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/esso.png" class="img-responsive">
            </div>
            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/band.png" class="img-responsive">
            </div>
            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/hsbc.png" class="img-responsive">
            </div>
            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/losango.png" class="img-responsive">
            </div>

            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/globo.png" class="img-responsive">
            </div>
            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/americanas.png" class="img-responsive">
            </div>
            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/esso.png" class="img-responsive">
            </div>
            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/band.png" class="img-responsive">
            </div>
            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/hsbc.png" class="img-responsive">
            </div>
            <div>
              <img src="<?php echo bloginfo('template_url'); ?>/img/losango.png" class="img-responsive">
            </div>
          </div>
        </div>        
      </div>
    </section>


    <?php get_template_part('includes/content','newsletter'); ?>

<?php get_footer(); ?>