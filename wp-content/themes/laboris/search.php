<?php
global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

if( strlen($query_string) > 0 ) {
  foreach($query_args as $key => $string) {
    $query_split = explode("=", $string);
    $search_query[$query_split[0]] = urldecode($query_split[1]);
  } // foreach
} //if

$search = new WP_Query($search_query);
get_header(); 
?>
    <div class="top-title-page">
      <div class="top-title text-center">
        <h1>Resultado de busca</h1>
      </div>
    </div>

    <div class="topo-busca">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-10 col-sm-push-1">
            <p>Exibindo resultados (<b><?php echo $wp_query->found_posts; ?></b>) para: <b><?php the_search_query(); ?></b></p>
          </div>
        </div>
      </div>
    </div>

    <section>
      <div class="container-fluid quem-somos busca">
        <div class="box-form-contato parceiro">
          <div class="row">
            <div class="col-sm-10 col-sm-push-1">
              <div class="box-busca scroll">
                <?php if($wp_query->found_posts > 0):  ?>
                  <ul>
                      <?php while ( have_posts() ) : the_post(); ?>
                      <li>
                          <a href="<?php the_permalink(); ?>">
                              <?php
                              $img = wp_get_attachment_image_src(get_post_thumbnail_id(),'thumbnail')[0];
                              if($img):
                              ?>
                              <img src="<?php echo $img; ?>">
                              <?php endif; ?>
                              <div class="text">
                                  <h4><i class="fa fa-circle"></i> NOTÍCIAS</h4>
                                  <h5><?php the_title(); ?></h5>
                                  <p><?php echo wp_strip_all_tags( get_the_content() ); ?></p>
                              </div>
                          </a>
                      </li>
                      <?php endwhile; ?>
                  </ul>

                  <?php if(!empty(get_next_posts_link())): ?>
                  <div class="row" style="margin-bottom: 30px;">
                    <div class="col-sm-12 text-center">
                      <a href="<?php echo get_next_posts_page_link() ?>" class="btn btn-danger btn-outline text-uppercase btn-plus next-page" style="width: 305px; display: inline-block; margin-top: 30px;">
                        Ver mais
                        <svg x="0px" y="0px" viewBox="0 0 400 400">
                        <g>
                          <g>
                            <path d="M199.995,0C89.716,0,0,89.72,0,200c0,110.279,89.716,200,199.995,200C310.277,400,400,310.279,400,200    C400,89.72,310.277,0,199.995,0z M199.995,373.77C104.182,373.77,26.23,295.816,26.23,200c0-95.817,77.951-173.77,173.765-173.77    c95.817,0,173.772,77.953,173.772,173.77C373.769,295.816,295.812,373.77,199.995,373.77z" fill="#D80027"/>
                            <path d="M279.478,186.884h-66.363V120.52c0-7.243-5.872-13.115-13.115-13.115s-13.115,5.873-13.115,13.115v66.368h-66.361    c-7.242,0-13.115,5.873-13.115,13.115c0,7.243,5.873,13.115,13.115,13.115h66.358v66.362c0,7.242,5.872,13.114,13.115,13.114    c7.242,0,13.115-5.872,13.115-13.114v-66.365h66.367c7.241,0,13.114-5.873,13.114-13.115    C292.593,192.757,286.72,186.884,279.478,186.884z" fill="#D80027"/>
                          </g>
                        </g>
                        </svg>
                      </a>
                    </div>
                  </div>
                  <?php endif; ?>

                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <?php get_template_part('includes/content','newsletter'); ?>
<?php get_footer(); ?>