<div class="container-fluid">
  <div class="news">
    <div class="top-title text-center">
      <h2>As melhores inovações do mercado no seu email</h2>
    </div>
    <div class="row">
      <div class="col-sm-8 col-sm-push-2">
        <?php echo do_shortcode('[contact-form-7 id="19" title="Newsletter"]'); ?>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
.news .wpcf7-not-valid-tip, .news .wpcf7-response-output {color: #fff !important;}
</style>