<?php 

function laboris_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'laboris_topo', array(
	   'title'    => 'Topo',
	   'priority' => 125,
	) );
	$wp_customize->add_setting( 'topo_logo_link', array(
	    'default'        => '',
	    'capability'     => 'edit_theme_options',	
	) );	
	$wp_customize->add_control( 'topo_logo_link', array(
	    'label'      => 'Logo - Link',
	    'section'    => 'laboris_topo',
	    'settings'   => 'topo_logo_link',
	) );
	$wp_customize->add_setting( 'topo_logo', array(
	    'default'        => '',
	    'capability'     => 'edit_theme_options',	
	) );	
	$wp_customize->add_control( new WP_Customize_Image_Control(
		$wp_customize,
		'topo_logo',
		array(
			'label'      => 'Logo',
			'section'    => 'laboris_topo',
			'settings'   => 'topo_logo',
		)
	) );

	$wp_customize->add_setting( 'topo_logo_white', array(
	    'default'        => '',
	    'capability'     => 'edit_theme_options',	
	) );	
	$wp_customize->add_control( new WP_Customize_Image_Control(
		$wp_customize,
		'topo_logo_white',
		array(
			'label'      => 'Logo Branca',
			'section'    => 'laboris_topo',
			'settings'   => 'topo_logo_white',
		)
	) );


	$wp_customize->add_section( 'laboris_rodape', array(
	   'title'    => 'Rodapé',
	   'priority' => 125,
	) );
	$wp_customize->add_setting( 'rodape_copyright', array(
	    'default'        => '',
	    'capability'     => 'edit_theme_options',
	) );
	$wp_customize->add_control( 'rodape_copyright', array(
	    'label'      => 'Copyright',
	    'section'    => 'laboris_rodape',
	    'settings'   => 'rodape_copyright',
	) );
}
add_action( 'customize_register', 'laboris_customize_register' );